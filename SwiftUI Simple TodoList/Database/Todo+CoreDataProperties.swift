//
//  Todo+CoreDataProperties.swift
//  SwiftUI Simple TodoList
//
//  Created by M Ramdhan Syahputra on 14/12/23.
//
//

import Foundation
import CoreData


extension Todo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Todo> {
        return NSFetchRequest<Todo>(entityName: "Todo")
    }

    @NSManaged public var status: String?
    @NSManaged public var date: Date?
    @NSManaged public var title: String?
    @NSManaged public var id: UUID?

}

extension Todo : Identifiable {

}
