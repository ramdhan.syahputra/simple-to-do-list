//
//  DataManager.swift
//  SwiftUI Simple TodoList
//
//  Created by M Ramdhan Syahputra on 14/12/23.
//

import CoreData
import Foundation

class DataManager: NSObject, ObservableObject {
    let container = NSPersistentContainer(name: "Todolist")
    
    @Published var todos: [Todo] = [Todo]()

    override init() {
        super.init()
        container.loadPersistentStores { _, _ in }
    }
}
